A helper for using Ultimate Cron on (some of) your Aegir hosted sites
=====================================================================

Aegir hostmaster 6.x-1.x is only compatible with Drush 4.5 and 4.6.

With Drush 4.x it is not possible for Ultimate Cron to hook itself
into the `drush cron` command in a way that will give you the full
benefit of using Ultimate Cron.

Since Aegirs cron handling executes `drush cron` you will not get the
full benefit if one or more of your Aegir hosted sites is using
Ultimate Cron.

This Drush extension provides you with a new Drush command for
executing `drush cron` or `drush cron-run` (whatever is appropriate
for each site) for each of your Aegir hosted sites.

You will need to disable Cron queues in Aegir at `admin/hosting/queues`.

Install the extension in `~aegir/.drush/ultimate_cron_aegir`.

Then add two new cronjobs for the `aegir` user (e.g. `crontab -u aegir
-e`) executing the new Drush command - something along this:

    0 * * * * drush @hostmaster --quiet uca-cron-run --skip-ultimate-cron
    * * * * * drush @hostmaster --quiet uca-cron-run --skip-core-cron

This will executed standard Drupal core cron on Aegir hosted sites
without Ultimate Cron once every hour and execute Ultimate Crons cron
handling on Aegir hosted sites with Ultimate Cron enabled every
minute.
