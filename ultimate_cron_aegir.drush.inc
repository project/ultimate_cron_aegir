<?php

/**
 * @file
 * A helper for using Ultimate Cron on (some of) your Aegir hosted sites.
 *
 * Disable Cron queues in Aegir at admin/hosting/queues.
 *
 * Add another cronjob executing this script - something along this:
 *
 * 0 * * * * drush @hostmaster uca-cron-run --skip-ultimate-cron
 * * * * * * drush @hostmaster uca-cron-run --skip-core-cron
 */

/**
 * Implements hook_drush_command().
 */
function ultimate_cron_aegir_drush_command() {
  $items = array();

  $items['uca-cron-run'] = array(
    'description' => "Run cron on Aegir with Ultimate Cron sites.",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array(
      'skip-ultimate-cron' => "Don't run Ultimate Crons cron.",
      'skip-core-cron' => "Don't run cores cron.",
    ),
  );

  return $items;
}

/**
 * Reimplements hosting_cron_queue().
 *
 * A changed implementation of hosting_cron_queue().
 *
 * Before executing "drush cron" on a site it will check to see if it is
 * possible to run Ultimate Crons "drush cron-run" instead.
 */
function drush_ultimate_cron_aegir_uca_cron_run($count = 20) {
  $result = db_query("SELECT n.nid FROM {node} n LEFT JOIN {hosting_site} s ON n.nid=s.nid WHERE n.type='site' and s.status = %d ORDER BY s.last_cron ASC, n.nid ASC", HOSTING_SITE_ENABLED);

  $i = 0;
  while ($i < $count && $nid = db_fetch_object($result)) {
    $site = node_load($nid->nid);
    $site_name = hosting_context_name($site->nid);

    if (variable_get('hosting_cron_use_backend', TRUE)) {
      $ultimate_cron_exists = provision_backend_invoke($site_name, 'cron-run', array(), array(
                                'help' => TRUE,
                                'backend' => TRUE,
                                '#integrate' => FALSE,
                              ));
      if (!$ultimate_cron_exists || in_array('DRUSH_COMMAND_NOT_FOUND', array_keys($ultimate_cron_exists['error_log']))) {
        if (!drush_get_option('skip-core-cron', FALSE)) {
          drush_log(dt('No Ultimate Cron for !site - running traditional cron.', array('!site' => $site_name)));
          provision_backend_invoke($site_name, 'cron');
        }
      }
      elseif (!drush_get_option('skip-ultimate-cron', FALSE)) {
        drush_log(dt('Ultimate Cron present for !site - using it!', array('!site' => $site_name)));
        provision_backend_invoke($site_name, 'cron-run');
      }
    }
    else {
      // Optionally add the cron_key querystring key if the site has one.
      $url = _hosting_site_url($site) . '/cron.php';
      if (!empty($site->cron_key)) {
        $url .= '?cron_key=' . rawurlencode($site->cron_key);
      }
      drush_log(dt('running cron on URL %url', array('%url' => $url)));
      $response = drupal_http_request($url);
      if (isset($response->error) && $response->error) {
        watchdog('hosting_cron', 'cron failed on site %site with error %error', array('%site' => $site->title, '%error' => $response->error), WATCHDOG_NOTICE);
        // Don't update the timestamp.
        continue;
      }
    }

    // We are updating the site table here directly to avoid a possible race
    // condition, with the task queue. There exists a chance that they might
    // both try to save the same node at the same time, and then an old record
    // from the cron queue might replace the newly updated record.
    db_query('UPDATE {hosting_site} SET last_cron=%d WHERE nid=%d', time(), $site->nid);
    $i++;
  }
}
